// Copyright 2021 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/golang/gddo/gddo-server/dynconfig"
	"github.com/golang/gddo/gddo-server/poller"
	"github.com/golang/gddo/log"
)

func newConfigPoller(ctx context.Context, pollEvery time.Duration) (_ *poller.Poller, err error) {
	location := os.Getenv("GDDO_CONFIG_LOCATION")
	if location == "" {
		return nil, fmt.Errorf("GDDO_CONFIG_LOCATION is not set")
	}
	log.Info(ctx, fmt.Sprintf("GDDO_CONFIG_LOCATION: %q", location))

	log.Info(ctx, fmt.Sprintf("using dynamic config from %s for experiments", location))
	getter := func(ctx context.Context) (*dynconfig.DynamicConfig, error) {
		return dynconfig.Read(ctx, location)
	}
	initial, err := getter(ctx)
	// If we can't load the initial state, then fail.
	if err != nil {
		return nil, err
	}
	p := poller.New(
		initial,
		func(ctx context.Context) (interface{}, error) {
			return getter(ctx)
		},
		func(err error) {
			// Log the error.
			log.Error(ctx, err.Error())
		})
	p.Start(ctx, pollEvery)
	return p, nil
}
